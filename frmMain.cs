﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;

namespace Hodiny
{
  public partial class frmMain : Form
  {
    public frmMain()
    {
      InitializeComponent();
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
      public int Left;
      public int Top;
      public int Right;
      public int Bottom;
    }
    public const int WM_SIZING = 0x0214;
    static int org_size = 0;
    protected override void WndProc(ref Message m)
    {
      if (m.Msg == WM_SIZING)
      {
        RECT rc;
        rc = (RECT)Marshal.PtrToStructure(m.LParam, typeof(RECT));

        if (rc.Right - rc.Left != org_size)
        {
          rc.Bottom = rc.Right - rc.Left + rc.Top;
          Marshal.StructureToPtr(rc, m.LParam, true);
        }
        else
        {
          rc.Right = rc.Bottom - rc.Top + rc.Left;
          Marshal.StructureToPtr(rc, m.LParam, true);
        }
        org_size = rc.Right - rc.Left;
      }

      base.WndProc(ref m);
    }
    
    public double ConvertToRadians(double angle)
    {
      return (Math.PI / 180) * angle;
    }

    private void frmMain_Paint(object sender, PaintEventArgs e)
    {
      Graphics g = e.Graphics;
      g.FillRectangle(new SolidBrush(Color.Gray), g.VisibleClipBounds);
      g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
      Pen bluePen = new Pen(Color.MidnightBlue, 10);   // hodinove carky
      Pen greenPen = new Pen(Color.Green, 2);          // minutove carky
      Pen blackPen = new Pen(Color.WhiteSmoke, 2);     // rucicka vterin
      Pen redPen = new Pen(Color.Red, 10);             // rucicka hodin a minut
      int width = (int)g.VisibleClipBounds.Width;
      int height = (int)g.VisibleClipBounds.Height;
      Point stred = new Point (width/2, height/2);
      int r = width / 2 - 20;
      DateTime now = DateTime.Now;
      int iHodina = now.Hour % 12;
      // cifernik
      g.FillEllipse(new SolidBrush(Color.White), stred.X - r, stred.Y - r, r * 2, r * 2);
      // gradient
      GraphicsPath gp = new GraphicsPath();
      gp.AddEllipse(g.VisibleClipBounds);
      PathGradientBrush pgb = new PathGradientBrush(gp);
      pgb.CenterPoint = stred;
      pgb.CenterColor = Color.White;
      pgb.SurroundColors = new Color[] { Color.Gray };
      e.Graphics.FillPath(pgb, gp);
      pgb.Dispose();
      gp.Dispose();
      // digitalky
      Font drawFont = new Font("LCD", 45);
      SolidBrush drawBrush = new SolidBrush(Color.Black);
      SizeF velikostDigitalek = g.MeasureString(now.ToLongTimeString(), drawFont);
      //Font tempusFont = new Font("Old English Text MT", 25);
      Font tempusFont = new Font("Magneto", 15);
      SolidBrush tempusBrush = new SolidBrush(Color.Gray);
      SizeF velikostTempus = g.MeasureString("Tempus Fugit", tempusFont);
      g.DrawString(now.ToLongTimeString(), drawFont, drawBrush, stred.X - (int)(velikostDigitalek.Width / 2),
        stred.Y + 50);
      g.DrawString("Tempus Fugit", tempusFont, tempusBrush, stred.X - (int)(velikostTempus.Width / 2),
        stred.Y - 100);

      // vykresleni pozic minut a hodin na ciferniku
      int uhel = 0;
      int x1, y1, x2, y2;
      for (int i = 0; i < 60; i++)
      {
        if (i > 0)
          uhel += 6;
        else
          uhel = 0;
        x2 = (int)(r * Math.Cos(ConvertToRadians(uhel)));
        y2 = (int)(r * Math.Sin(ConvertToRadians(uhel)));
        if (i % 5 == 0)
        {
          if (i == 0 || i == 15 || i == 30 || i == 45)
          {
            // hodinove pozice
            x1 = (int)((r - 45) * Math.Cos(ConvertToRadians(uhel)));
            y1 = (int)((r - 45) * Math.Sin(ConvertToRadians(uhel)));
            g.DrawLine(bluePen, new Point(x1 + stred.X, y1 + stred.Y), new Point(x2 + stred.X, y2 + stred.Y));
          }
          else
          {
            // hodinove pozice
            x1 = (int)((r - 30) * Math.Cos(ConvertToRadians(uhel)));
            y1 = (int)((r - 30) * Math.Sin(ConvertToRadians(uhel)));
            g.DrawLine(bluePen, new Point(x1 + stred.X, y1 + stred.Y), new Point(x2 + stred.X, y2 + stred.Y));
          }
        }
        else
        {
          // minutove pozice
          x1 = (int)((r - 20) * Math.Cos(ConvertToRadians(uhel)));
          y1 = (int)((r - 20) * Math.Sin(ConvertToRadians(uhel)));
          g.DrawLine(greenPen, new Point(x1 + stred.X, y1 + stred.Y), new Point(x2 + stred.X, y2 + stred.Y));
        }
      }
      // vykresleni rucicek
      // minuty
      uhel = 360;
      if (now.Minute != 0)
        uhel = (now.Minute * 360 / 60);
      x1 = 0;
      y1 = 0;
      x2 = (int)((r - 10) * Math.Cos(ConvertToRadians(uhel - 90)));
      y2 = (int)((r - 10) * Math.Sin(ConvertToRadians(uhel - 90)));
      g.DrawLine(redPen, stred, new Point(x2 + stred.X, y2 + stred.Y));
      // bambule
      //Point stred_minutky_bambule = new Point((int)((r - 10) * Math.Cos(ConvertToRadians(uhel - 90))) + stred.X,
      //  (int)((r - 10) * Math.Sin(ConvertToRadians(uhel - 90))) + stred.Y);
      //g.FillEllipse(new SolidBrush(Color.Yellow), stred_minutky_bambule.X - 25, stred_minutky_bambule.Y - 25, 25 * 2, 25 * 2);

      // hodiny - hodinova rucicka musi mit presah - napr. kdy je 12:55,
      // tak hodinova rucicka neni na 12ce, ale skoro na 1
      uhel = 360;
      double iUhlovyPosun = (double)(30 / ((360 / (double)(now.Minute * 360 / 60))));
      if (iHodina != 0)
        uhel = (30 * iHodina) + (int)iUhlovyPosun;
      x1 = 0;
      y1 = 0;
      x2 = (int)((r - 70) * Math.Cos(ConvertToRadians(uhel - 90)));
      y2 = (int)((r - 70) * Math.Sin(ConvertToRadians(uhel - 90)));
      g.DrawLine(redPen, stred, new Point(x2 + stred.X, y2 + stred.Y));
      // bambule
      //Point stred_hodiny_bambule = new Point((int)((r - 80) * Math.Cos(ConvertToRadians(uhel - 90))) + stred.X,
      //  (int)((r - 80) * Math.Sin(ConvertToRadians(uhel - 90))) + stred.Y);
      //g.FillEllipse(new SolidBrush(Color.Yellow), stred_hodiny_bambule.X - 25, stred_hodiny_bambule.Y - 25, 25 * 2, 25 * 2);

      // vteriny
      uhel = 360;
      if (now.Second != 0)
        uhel = (now.Second * 360 / 60);
      x1 = 0;
      y1 = 0;
      x2 = (int)((r - 10) * Math.Cos(ConvertToRadians(uhel - 90)));
      y2 = (int)((r - 10) * Math.Sin(ConvertToRadians(uhel - 90)));
      g.DrawLine(blackPen, stred, new Point(x2 + stred.X, y2 + stred.Y));
      // bambule
      Point stred_hodiny_bambule = new Point((int)((r - 70) * Math.Cos(ConvertToRadians(uhel - 90))) + stred.X,
        (int)((r - 70) * Math.Sin(ConvertToRadians(uhel - 90))) + stred.Y);
      g.FillEllipse(new SolidBrush(Color.WhiteSmoke), stred_hodiny_bambule.X - 25, stred_hodiny_bambule.Y - 25, 25 * 2, 25 * 2);

      // kolecko uprostred ciferniku
      g.FillEllipse(new SolidBrush(Color.Black), stred.X - 10, stred.Y - 10, 10 * 2, 10 * 2);
    }

    private void timer_Tick(object sender, EventArgs e)
    {
      Refresh();
    }
  }
}
